function [K_sil, all_assignments, wss] = find_optimal_K(X, maxClusters)
    wss = zeros(maxClusters,1);
    avgS = zeros(maxClusters,1);
    all_assignments = zeros(maxClusters,size(X,1));
    
    wss(1) = compute_within_cluster_sum_squares(X, ones(size(X,1),1), 1);
    for k=2:maxClusters
        disp(k);
        cidx = kmeans(X,k);
        all_assignments(k,:) = cidx;
        wss(k) = compute_within_cluster_sum_squares(X, cidx, k);
        avgS(k) = mean(silhouette(X,cidx));
    end
    
    figure;
    subplot(2,1,1);
    plot(1:maxClusters,wss);
    title('Within Cluster Variation (Elbow Method)');
    subplot(2,1,2);
    plot(1:maxClusters,avgS);
    title('Average Silhouette (Silhouette Method)');
    
    [~,K_sil] = max(avgS);
end

function [wss] = compute_within_cluster_sum_squares(X, cidx, K)
    wss = 0;
    for k=1:K
        % compute mean of each cluster
        cluster_points = X(cidx == k, :);
        center = mean(cluster_points);
        % compute sum of euclidean distances to that center
        for i=1:size(cluster_points,1)
            wss = wss + sum((cluster_points(i,:) - center).^2);
        end
    end
end