function [adj] = create_adjacency_matrix(edgeWeights, numNodes)
    gene_sums = sum(abs(edgeWeights));
    assert(numel(gene_sums) == (numNodes * (numNodes - 1) / 2));
    
    adj = zeros(numNodes, numNodes);
    k = 1;
    for i=1:(numNodes-1)
        for j=(i+1):numNodes
            adj(i,j) = (gene_sums(k) > 0);
            k = k + 1;
        end
    end
    
    adj = adj + adj';
end