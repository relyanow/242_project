% imputeData:
%   originalData : NxP, where N is number of samples, P is features
%       in our case, N=# cells, P=# genes (so ~3593x1000)
%   numClusters : number of clusters for kmeans
%   multipleImpute : number of samples to do for each imputation
%   verbose : optional parameter. 1 if you want to print out some sample
%       histograms and other statistics
function [imputedData] = imputeMissingData(originalData, numClusters, multipleImpute, verbose)
    if nargin < 3
        multipleImpute = 1;
    end
    if nargin < 4
        verbose = 0;
    end
    
    %% run kmeans
    clusters = kmeans(originalData, numClusters);
    
    %% impute missing data in each cluster
    imputedData = originalData;
    numCells = size(originalData, 1);
    numGenes = size(originalData, 2);
    usedPoisson = zeros(numGenes, numClusters);
    numImputed = zeros(numGenes,1);
    
    for i = 1:numGenes
        % impute missing data for cells w/o count for gene i
        d = originalData(:,i); % get how cells are expressed for gene i
        nonZero = (d > 0);
        
        for k = 1:numClusters
            clusterK = (clusters == k);
            
            numToImpute = numel(d(clusterK & ~nonZero));
            numImputed(i) = numImputed(i) + numToImpute;
            dk = d(clusterK & nonZero);
            
            if numel(dk) == 0
                % no other data with this cluster, so leave as 0
                continue
            end
            
            if mean(dk) > var(dk)
                [params] = poissfit(dk); % fit poisson to it           
                imputedData(clusterK & ~nonZero, i) = mean(poissrnd(params, numToImpute, multipleImpute), 2);
                usedPoisson(i, k) = 1;
            else
                [params] = nbinfit(dk); % fit negbinomial to it (if there is enough data)           
                imputedData(clusterK & ~nonZero, i) = mean(nbinrnd(params(1), params(2), numToImpute, multipleImpute), 2);
            end
        end
    end
    
    if verbose
        disp('Results');
        numUsedPoisson = sum(usedPoisson(:)); 
        fprintf('Imputed %d of %d total values (%f%%)\n', sum(numImputed), numel(originalData), 100*sum(numImputed)/numel(originalData));
        fprintf('Number of poisson fits: %d, number of negative binomial fits: %d\n\n', numUsedPoisson, numel(usedPoisson)-numUsedPoisson);
        
        numRandomToPlot = 5;
        plotHistos = [ 1 10 12 ];
        randomGenes = datasample((max(plotHistos)+1):numGenes, numRandomToPlot, 'Replace', false);
        plotHistos = [plotHistos sort(randomGenes)];
        
        for g = plotHistos
            figure;
            fprintf('Analysis of imputation for gene %d\n', g);
            fprintf('Imputed %d of %d total values (%f%%)\n', numImputed(g), numCells, 100*numImputed(g)/numCells);
            fprintf('Number of poisson fits for gene %d: %d, number of negative binomial fits: %d\n', g, sum(usedPoisson(g,:)), sum(usedPoisson(g,:)==0));
            
            od = originalData(:,g);
            od = od(od > 0);
            
            subplot(2,1,1);
            hist(od,max(od)+1)
            title(sprintf('Original Data for gene %d', g));
            
            subplot(2,1,2);
            hist(imputedData(:,g),max(imputedData(:,g))+1)
            title(sprintf('After Imputation Data for gene %d', g));
            
            fprintf('\n');
        end
    end
end