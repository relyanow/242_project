PATH_TO_THESIS_CODE='/home/svatasoiu/git/242_project/thesis';
PATH_TO_GENETIC_DATA='/home/svatasoiu/git/242_project/gene_by_barcode_matrix.txt';
NUM_THREADS = 6;

if exist('original_data','var') == 0
    addpath(genpath(PATH_TO_THESIS_CODE))
    original_data = csvread(PATH_TO_GENETIC_DATA);
end

data = 1 + original_data;

max_value = 20;
data(data > max_value) = max_value;
data(data == 0) = 0;
    
numGenes = 20;
numTrain = 200;
numValidate = 50;
numTest  = 50;

trainData = data(1:numGenes,1:numTrain)';
validateData  = data(1:numGenes,numTrain+(1:numValidate))';

% all interesting options
lambdas = 2.^(-7:10);
% infer_methods = {'pseudo' 'mean' 'loopy'};
% regularizers = {'1' '2' 'G' 'I' 'N'};
% model_types = {'I' 'P'};

disp('Getting a cluster + thread pool');
tic
c = parcluster;
maxNumCompThreads(max(NUM_THREADS, maxNumCompThreads));
c.NumWorkers = maxNumCompThreads
parpool(c)
toc

disp('Running models');

infer_methods = {'pseudo'};
regularizers = {'G' 'I'};
model_types = {'P'};

% clear variables
clear models; clear minNLLs; clear minOpts; clear minModels;

models = {};
minNLLs = zeros(numel(model_types), 1) + Inf;

comboNum = 0;
totalCombos = numel(model_types) * numel(infer_methods) * numel(regularizers);

for imt = 1:numel(model_types)
    model_type = model_types{imt};
    for im = 1:numel(infer_methods)
        inference_method = infer_methods{im};
        for ir = 1:numel(regularizers)
            regularizer = regularizers{ir};
            
            m = {};
            
            comboNum = comboNum + 1;
            fprintf('Progress for combination %d/%d:\n', comboNum, totalCombos);
            
                op = struct;
                op.param = model_type;
                op.infer = inference_method;
                op.retype = regularizer;
                disp(op)
                
            fprintf([repmat('.',1,numel(lambdas)) '\n\n']);
            
            tic
            parfor il = 1:numel(lambdas)
                lambda = lambdas(il);
                
                % set up options
                options = struct;
                options.param = model_type;
                options.infer = inference_method;
                options.regType = regularizer;
                options.lambda = lambda;
                options.verbose = 0;

                m{il} = LLM2_train(trainData, options);
                fprintf('\b|\n');
            end
            fprintf('Time for option combo %d: ', comboNum);
            toc
            fprintf('\n');
            
            models{imt}{im}{ir} = m;
        end
    end
end
fprintf('\n');

% computing NLL's for all options
for imt = 1:numel(model_types)
    model_type = model_types{imt};
    for im = 1:numel(infer_methods)
        inference_method = infer_methods{im};
        for ir = 1:numel(regularizers)
            regularizer = regularizers{ir};
            for il = 1:numel(lambdas)
                lambda = lambdas(il);
                
                nll = models{imt}{im}{ir}{il}.nll(models{imt}{im}{ir}{il}, validateData, 'pseudo');
                if nll < minNLLs(imt)
                    minNLLs(imt) = nll;
                    
                    options = struct;
                    options.param = model_type;
                    options.infer = inference_method;
                    options.retype = regularizer;
                    options.lambda = lambda;
                    
                    minOpts{imt} = options;
                    minModels{imt} = models{imt}{im}{ir}{il};
                end
            end
        end
    end
end

%% compute number of non-zero weights, i.e., num of edges remaining
for imt = 1:numel(model_types)
    % print results
    disp('RESULTS');
    fprintf('Best NLL for model %s is %f, for options:\n', model_types{imt}, minNLLs(imt));
    disp(minOpts{imt});
    disp(minModels{imt});
    plot(minModels{imt}.fs);
    
    % reshape weights + find # of edges removed/remaining
    weights = minModels{imt}.w
    nNodes = numGenes;
    nStates = max(trainData(:)); % should be 21 (1 + 20=max_value)
    nEdges  = nNodes * (nNodes - 1) / 2;
        
    switch model_types{imt}
        case 'I'
            edgeWeights = reshape(weights((nNodes*(nStates-1))+1:end), nEdges, 1);
        case 'P'
            edgeWeights = reshape(weights((nNodes*(nStates-1))+1:end), nStates, nEdges);
    end
    
    disp(edgeWeights)
end
% clean up thread pool
delete(gcp);