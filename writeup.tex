\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
% \PassOptionsToPackage{numbers, compress}{natbib}
% before loading nips_2016
%
% to avoid loading the natbib package, add option nonatbib:
% \usepackage[nonatbib]{nips_2016}



% to compile a camera-ready version, add the [final] option, e.g.:
% \usepackage[final]{nips_2016}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{graphicx}
\usepackage{microtype}      % microtypography
\usepackage{amsmath}
\usepackage{subcaption}
\usepackage[final]{nips_2016} % produce camera-ready copy
\title{242 Final Project: Learning pairwise MRFs for single-cell sequencing data}

% The \author macro works with any number of authors. There are two
% commands used to separate the names and addresses of multiple
% authors: \And and \AND.
%
% Using \And between authors leaves it to LaTeX to determine where to
% break the lines. Using \AND forces a line break at that point. So,
% if LaTeX puts 3 of 4 authors names on the first line, and the last
% on the second line, try using \AND instead of \And before the third
% author name.

\author{
 Rebecca Elyanow and Sorin Vatasoiu\\
  \texttt{} \\
  %% examples of more authors
  %% \And
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
  %% \AND
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
  %% \And
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
  %% \And
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
}

\begin{document}
% \nipsfinalcopy is no longer used

\maketitle

%\begin{abstract}
%
%\end{abstract}

\section{Description of problem}
Over the course of our lives our cells accumulate somatic mutations in their genomes. A cell becomes cancerous when it acquires a mutation or mutations that lead to uncontrolled growth. A mutation can have a wide range of varying effects on a cell's phenotype, making it difficult to predict its association to disease progression [1]. Ultimately, the function of a cell is determined by the types and amounts of proteins produced by that cell, which can be measured by sequencing the RNA corresponding to each expressed gene. \\

Cells within the same tumor may harbor different mutations and express different genes. Such intratumor heterogeneity is becoming increasingly appreciated as a determining factor in treatment failure and disease recurrence [2]. The majority of research in cancer genomics is performed using bulk sequencing data. In bulk sequencing, DNA or RNA from all cells within a tumor sample are pooled together prior to sequencing. This pooling process makes it difficult to understand tumor heterogeneity and can make discovering cancer-associated genes difficult.\\

We aim to use single-cell RNA sequencing data to identify clusters of genes sharing similar functional properties or being connected by the same pathway. We expect such genes to show similar expression across all or many cells. For example, if genes A and B are both members of the 'cell proliferation' pathway, then we would expect that when A is upregulated (overexpressed) B is also upregulated and when A is downregulated (underexpressed) B is also downregulated.

\section{Related work}
In the past few years, several studies have been performed using single-cell RNA sequencing. In one study, clusters of glioblastoma cells were identified using hierarchical clustering of expression profiles [2]. Another study performed PCA and k-means clustering of expression profiles to identify clusters of cells[3]. Graphical models have been used to classify genes and proteins [5].Typically, in these models, nodes represent genes or proteins while edges represent relationships between the genes or proteins (such as physical relationships, pathway-based relationships, or relationships based on expert knowledge). 

\section{Description of the data}
The data we will apply to our model consists of single-cell RNA-sequencing data from bone marrow mononuclear cells (BMMCs) from an acute myeloid leukemia (AML) tumor and BMCCs from a healthy control. Each sample consists of RNA transcripts from about 4000 individual cells. Two samples were taken from each AML patient, one pre-transplant and one post-transplant. In this single-cell RNA-seq data, RNA transcripts are sequenced to low coverage, meaning that for each cell we only have data for about $3\%$ of all genes. For each sequenced gene, we have a count of the number of transcripts matching that gene, which can be interpreted as the \textit{expression level} of that gene in a specific cell.

To visualize how varied this data is, we include some descriptive histograms in the appendix. 

Figure \ref{gene_sparsity_histogram} shows the sparsity (in terms of fraction of cells that are 0 for a given gene) of all genes. 
As is clear, the sparsity ranges from near 0\% to over 99\% for individual genes. Even when genes and cells do have data,
the other two histograms (figures \ref{gene_count_histogram} and \ref{cell_count_histogram}) show how skewed these counts are, with some genes having single digit amounts of data, and others having
on the order of hundreds of thousands.

\section{Prior work in the area}
%Maybe this: \url{https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4523298/}
\includegraphics[scale=.5]{figures/fig1}
\\Our data set comes from a recently published study on "Massively parallel digital transcriptional profiling of single cells." [3] In this study, Zheng et al. identify several distinct cell types in a population of post-transplant bone marrow mononuclear cells (BMMCs) using PCA and k-means clustering. The results are shown in figure 1. 20\% of the cells from one post-transplant AML sample were categorized as immature granulocytes based on their RNA expression profiles, indicating that the patient maintained a population of cancerous cells post-transplant. This prediction was confirmed by clinical assay. This result shows that it is possible to identify significant subpopulations of tumor cells by clustering RNA-seq data. However, Zheng et al. noted that they could not determine finer scale functional relationships between cells due to their varied and overlapping functions. They expressed the need for more sophisticated clustering and classification methods to help address these problems.

\section{Challenges}
Several challenges must be overcome before applying the model to our data. RNA transcripts from each cell are only sequenced for a small subset of genes. Thus, observing no transcripts could either mean that a gene was significantly underexpressed, or that the transcripts are simply missing by chance. Additionally, we would like to learn a model that prioritizes high connectivity between genes in the same pathway and sparsity between genes in different pathways, but we don't know the pathways or the number of pathways a priori.

Another challenge is the scale of the data. Exact learning is impractical as the number of features in our case is the number 
of genes, which is $500+$. To overcome this, we tried using different approximations, mainly the methods of mean field variation 
and pseudolikelihood. Mean field approximations aim to lowerbound the likelihood by variational approximations of a simpler form. 
The pseudolikelihood method aim to approximate the joint likelihood by the product of univariate conditional likelihoods. It has 
been proven that the latter method is consistent so as the number of training examples grows, it approaches the 
true likelihood. Mathematically, the pseudolikelihood objective is [6]:

$$\min_{w,b} -\sum_{k=1}^m[\sum_{i=1}^p\log p(x_i^m|x_{-i}^m,w,b)]+\lambda\|w\|_1$$

Where,

$$p(x_i|x_{-i},w,b)=\frac{1}{Z_i}\phi_i(x_i,b_i)\prod_{j\neq i}\phi_{ij}(x_{ij},w_{ij})$$

Where, $Z_i$ is a tractable normalizing constant that sums over all possible values for $x_i$. 
This means that the approximation is essentially linear in the number of possibles states as opposed to exponential 
in the case of exact inference.

\section{Imputation}
Given that over half of our data is missing (missing as in a count of 0 for a given gene/cell combination). 
While it may be the case that some of these are actually meant to be 0's, it is clear that many are missing. 
To make matters worse, data within each individual gene is also rather varied with some genes having counts on the 
order of hundreds of thousands across all cells to some cells with counts in the single digits. 
To try and alleviate some of the issues, we chose to experiment with imputing the missing values. 

There are a few different ways of doing this. The easiest of which would be to just set the missing data within a gene
to be the mean of all the counts within that gene, not counting those 0-valued cells. This causes several issues: 
\begin{itemize}
 \item All the imputed values will be the same within a gene
 \item We only use the information about the distribution's first moment.
 \item We don't allow for the possibility of imputing zero values (unless of course the mean is 0).
\end{itemize}

We also are concerned that by imputing a count using all the cell/gene count observations for a single gene,
we are assuming that all cells are coming from the same distribution. To get around these issues, 
we chose to first perform a clustering (for simplicity, we use k-means), and then impute a count 
from the distribution of the other cells within the same cluster. 

So, our algorithm for imputing the missing values (where the count for the $g^{th}$ gene and $i^{th}$ cell is $x_g^{(i)}$) 
is as follows:
\begin{enumerate}
 \item Choose a $K$, the number of clusters. (See the next section for this process)
 \item Perform k-means clustering which assigns a cluster to each of the $N$ cells.
 \item Sample $x_g^{(i)}$ from a distribution learned from $x_g^{(j)}$ such that cell $j$ is in the same cluster 
 as cell $i$ and $x_g^{(j)} > 0$ (i.e. not missing). 
\end{enumerate}

The choices for this distribution could range from simply a degenerate distribution where the mean of the $x_g^{(j)}$ takes 
probability one, to a more complicated structure in which we assume the distribution belongs to a certain class of families. 
In our case, after inspecting a few histograms, it was clear that a poisson or negative binomial model would make sense.
Additionally, these two distributions naturally fit with trying to sample counts. Also, if we choose some discrete distribution,
we allow the possibility of sampling 0 for a count, which is consistent with what the data represents. We used a negative binomial
fit whenever possible, but in cases where the sample variance is greater than its mean, Matlab cannot fit the negative binomial model 
(using nbinfit to fit and nbinrnd to sample), so we resorted to using a poisson distribution (poissfit to fit and poissrnd to sample).

To evaluate how well the imputation worked, we plotted CDF's and histograms before and after imputation. 
See the appendix for these plots (figures \ref{mean_imputation_10} to \ref{cdf_group_imputation_12}).

\section{Selection of k}
The process for selecting a good choice of $K$, the number of clusters to use, is described in Marlin and Murphy's paper [5]. 
The idea is to start with all cells in the same cluster and then continuously split a cluster until splitting no longer helps 
or we have run for enough iterations. Throughout this process, our proxy for how "good'' a clustering is is based on 
the variational free energy of the clustering. See the pseudo code in figure \ref{pseudo_code} for more details.

\begin{figure}[h]
  \centering
    \includegraphics[width=.7\textwidth]{figures/pseudo_code}
   \caption{Pseudo Code for selecting $K$}
   \label{pseudo_code}
\end{figure}
The above method turned out to be too complicated to implement (too complicated for the minimal advantage it gives us). Instead of using this method,
we just implemented the elbow and silhouette methods for determining the right number of clusters for k-means clustering. These
methods were implemented in Matlab. The elbow method is a visual method for picking the right $K$ in which the 
total within-cluster variation is computed several values of $K$, and plotted against $K$ in figure \ref{elbow_method_plot}. 
The $K$ at which the plot ``elbows'' out is chosen to be the best $K$.
The silhouette method aims to select $K$ based on how well each variable fits into its assigned cluster. 
The average silhoutte statistic is also plotted in figure \ref{elbow_method_plot} for several $K$. 
The $K$ that gives the highest average is selected. From the plots, the best $K$ to use are either 3 or 4.

\begin{figure}[h]
  \centering
    \includegraphics[width=.7\textwidth]{figures/optimal_K_by_cell}
   \caption{Plot of the total within-cluster variation and Average Silhouette for $K=1...10$}
   \label{elbow_method_plot}
\end{figure}

\section{Parameter and structure learning estimation via Group-$\ell_1$ Regularization}
We propose to model the interaction between genes using the Generalized Ising model. The vanilla Ising model 
assumes a connection between every pair of nodes. In the GIsing model, there is an edge between each 
pair of nodes for each possible state of the joined nodes. As in the Ising model, we learn these edge weights 
and determine that those with non-zero edge weight indicate a relevant connection between two genes. 

However, in the GIsing model, we can only conclude that there is an edge between to nodes if the learning algorithm
fits 0's to all edges between a pair of nodes. 
Since this would be very unlikely if the learning process used just $\ell_1$ regularization (which doesn't know anything about groups), 
we use Group $\ell_1$ Regularization as proposed by Mark Schmidt. 
The typical $\ell_1$ regularization term is $\lambda \|\bold{w}\|_1$, where $\bold{w}$ is the vector of edge weights.
The corresponding Group $\ell_1$ Regularization term is $$\lambda \sum_{i=1}^N\sum_{j=i+1}^N\|\bold{w_{i,j}}\|_p$$
where $p$ is some $\ell_p$ norm and $\bold{w_{i,j}}$ is a vector of all edge potentials between two nodes (a vector of length equal to the total number of 
possible states). The choise of which $p$-norm is not clear, but two main ones used are $\ell_2$ and $\ell_\infty$ [6]. 

\section{Parameter and block estimation via GRAB}
Our dataset contains many variables (genes) that are densely connected to each other through pathways. Genes in the same pathway will have similar expression patterns while genes in unrelated pathways will likely have very different expression patterns, leading to a graph structure that contains several dense clusters, or \textit{blocks}, of highly connected genes and relatively sparse connections between these blocks. This type of model is known as a 'stochstic block model'. The stochastic block model assigns the probability of an edge between $i$ and $j$ based on the block assignment of nodes $i$ and $j$ and the probability of an edge between members of their respective blocks. The $k\times k$ matrix $\pi$ contains the probabilities of edges between blocks, while the $D\times k$ matrix $Z$ contains the probabilities of block assignment for each variable. The $D\times D$ matrix $ZZ^T$ expresses the probability that variables $i$ and $j$ are in the same block. $\pi$ and $ZZ^T$ are used to learn the graph structure, $\theta$, which is a $D\times D$ matrix of edge probabilities, where 0 weight entries indicate no edge between two variables [5][6]. A diagram of the model is shown in figure \ref{blockmodel}.

\begin{figure}[h]
  \centering
    \includegraphics[width=.3\textwidth]{figures/blockmodel}
   \caption{$\pi$ contains the probabilities of edges between blocks. $ZZ^T$ contains the similarities between variables (variables are most similar if they are in the same block).}
   \label{blockmodel}
\end{figure}
The GRAB model (GRaphical models with overlApping Blocks) [4] builds upon the stochastic block model framework by allowing variables to be assigned multiple blocks. This is an important feature for gene network interactions, because genes are often members of multiple pathways and allowing genes to be assign to multiple pathways will allow for the discovery of more accurate and realistic gene interaction networks. \\
\\
Since we want to identify clusters of genes that share similar expression profiles across a population of cells, a natural approach is to try and learn a sparse graph structure from a fully connected graph of all genes. The nodes in this graph will correspond to the genes in our dataset and the edges will indicate pairwise dependencies between genes. Using $\mathcal{L}^1$ regularization, we hope to identify the most strongly connected clusters of genes.\\
\\
Mathematically, let $X_{ig}$ be the expression of gene $g$ in cell $i$. Where $X_{ig}$ is a positive integer representing the number of transcripts sequenced for gene $g$ in cell $i$ (more sequenced transcripts corresponds to higher expression and fewer sequenced transcripts corresponds to lower expression). Genes with no sequenced transcripts will have $X_{ig} = 0$. This would cause increased likelihood for features that are expressed most similarly. This way, we could just do $x_sx_t$ instead of indicator functions. \\
  \\
Let $\mathcal{V}$ be the set of all genes and $\mathcal{E}$ be the set of all pairs of genes. Using a fully connected, pairwise Markov Random Field and a Laplacian Prior (i.e. $\mathcal{L}^1$ regularization), our joint distribution is:
$$p(x|\theta) = \exp(\sum_{s\in\mathcal{V}}\theta_sx_s + \sum_{(s,t)\in\mathcal{E}}\theta_{st}^{(1)}\mathbb{I}(x_s=x_t=1) + \sum_{(s,t)\in\mathcal{E}}\theta_{st}^{(2)}\mathbb{I}(x_s=x_t=2) - \phi(\theta))$$
$$p(\theta|\lambda) = \prod_{s\in\mathcal{V}}Lap(\theta_s|\lambda)
\prod_{(s,t)\in\mathcal{E}}Lap(\theta_{st}^{(1)}|\lambda)Lap(\theta_{st}^{(2)}|\lambda)$$
where $Lap(\theta|\lambda)$ is the pdf of the Laplacian distribution with parameter $\lambda$.\\
\\
We aim to learn both the block structure, $Z$, and the precision matrix, $\theta$ for a markov random field with overlapping blocks. Let $X = \{ X_1,...,X_D\}$ be the variables (genes) in the networks, where $X_i$ is a vector of transcript counts over $n$ cells. Let $Z$ be a real matrix of size $D \times k$, where $k$ is the total number of blocks. Each element, $Z_{ik}$ is a score from $-1$ to $1$ that represents the likelihood of variable $i$ belonging to block $k$ and $ZZ^T_{ij}$ represents the similarity of variables $i$ and $j$ with respect to the block structure. The GRAB model maximizes the following equation:
\begin{equation}
\max_{\theta > 0,Z} \log \det \theta - tr(S \theta) - \lambda(\parallel \theta \parallel_l-tr(ZZ^T|\theta|)
\end{equation}
The prior $tr(ZZ^T|\theta|)$ encourages edges between variables with similar embeddings (and sparsity between other variables). Learning of $Z$ and $\theta$ is performed using EM. In the $\theta$-step, $\theta$ is estimated from the current $Z$ and in the $Z$-step, $Z$ is estimated from the current theta. These steps are performed until the objective function converges. The objective function is the value of the following equation when $\theta$ is maximized:
\begin{equation}
\max_{\theta > 0} \log \det \theta - tr(S \theta) - \sum_{ij} \lambda(1-ZZ^T_{ij})|\theta_{ij}|	
\end{equation}
The $Z$-step involves learning $Z$ given $\theta$ by:
\begin{equation}
\max_{ZZ^T > 0} tr((ZZ^T)|\theta|)
\end{equation}
The $\theta$-step and $Z$-step as described in [4] were already implemented in the function 'GRAB.py', which we downloaded from the authors' webpage. We updated the code in the function 'main.py' to find the covariance of our data and apply the  $\theta$-step and $Z$-steps until convergence. We also tried modifying the covariance matrix by only including covariances between entries that were both non-zero in order to attempt to circumvent errors introduced by missing data. 
\begin{equation}
\begin{split}
\Sigma_{ij} =& cov(X_i,X_j) = E[(X_i-\mu_i)(X_j-\mu_j)],\\
& X_i,X_i = [x_{i1},...,x_{im}],[x_{j1},...,x_{jm}] ~~s.t. ~~x_{i1} \neq 0 \text{ and } x_{j1} \neq 0
\end{split}
\end{equation}
This produced results that were slightly better than using the data with missing values but worse than performing imputation. In figure \ref{objective}, we plot the objective function with respect to the number of iterations. The objective is monotonically increasing.
\begin{figure}[h]
  \centering
    \includegraphics[width=.5\textwidth]{figures/objective}
   \caption{Plot of the objective function vs. the number of iterations. Objective is monotonically increasing. This is specifically the objective using the imputed data, but results running on non-imputed data shows the same trend.  }
   \label{objective}
\end{figure}

\section{Results}
The results for using the Group-$\ell_1$ Regularization weren't very useful. We could not infer any specific groups of genes as most genes
were well connected with others. We ran the Schmidt Group-$\ell_1$ Regularization with regularization constants ($\lambda$) ranging 
from $2^{-5}$ to $2^5$, with both the $\ell_2$ and $\ell_\infty$ norms, and the pseudolikelihood approximation on the first 100 genes.
The $\lambda$ which resulted in the objective function value was $\lambda = 8$ and the best norm was the $\ell_2$ norm. 
The resulting objective function is plotted in \ref{model_plot} and the corresponding graph structure that is learned is 
in \ref{network_graph}. We used Leon Peshkin's GraphViz interface for Matlab to display the learned graph structure [8]. 
Since the graph structure did not give us much information, we decided to focus on using the GRAB model. 

\begin{figure}[h]
  \centering
    \includegraphics[width=.5\textwidth]{figures/model_plot}
   \caption{Plot of Objective Function for Group-$\ell_1$ Regularization Method with $\lambda=8$, the $\ell_2$ norm and the pseudolikelihood approximation vs number of iterations. }
   \label{model_plot}
\end{figure}
\begin{figure}[h]
  \centering
    \includegraphics[width=.7\textwidth]{figures/network_graph_100}
   \caption{Visualization of graph learned using the Group-$\ell_1$ Regularization Method with $\lambda=8$, the $\ell_2$ norm and the pseudolikelihood approximation. }
   \label{network_graph}
\end{figure}

First, we selected the top 1000 most variable genes in a dataset containing 3592 cells and 23,039 genes. We then imputed missing values using the method described above. We performed block and parameter estimation via the GRAB method on a subset of the data, containing expression levels for 500 genes across 500 cells for both the imputed data and the original data containing missing values. Since there is no gold standard of gene assignments to blocks, there is no way to measure the true accuracy of the the block assignments obtained using the GRAB method. However, this is common issue among researchers conducting gene expression analysis. A common practice is to perform "gene set enrichment analysis" on the resulting sets of genes (where each block is a set). \\
\\
Gene sets that correspond to some known function or pathway should be significantly enriched for genes from known, canonical, pathways. We downloaded a library containing  1,329 canonical pathways from the Molecular Signatures Database and obtained p-values for the enrichment of each gene set (block) with each canonical pathway by testing for gene matches between the test set and 200 random gene sets of the same size. For each gene set, pathways with p-value $>0.01$ were considered enriched. Assuming each block represents a functionally important set of genes, we would expect a higher number of significantly enriched pathways than if genes in a block did not share some common functionality. Thus, we use the number of enriched pathways as a measurement of the success of the block assignment. We performed GRAB on the original data and the imputed data with $k=10$ blocks and $\lambda = 0.05$.  \\
\\
As shown in figure \ref{gene_enrichment}, the number of enriched genes sets increases approximately 3-fold when the GRAB model is applied to the imputed data versus the non-imputed data. Both the imputed and non-imputed block assignments identify significantly more gene sets than the background level, where genes are randomly assigned to blocks. 
\\

\begin{figure}[h]
  \centering
    \includegraphics[width=.7\textwidth]{figures/gene_enrichment}
   \caption{Gene set enrichment analysis on blocks of genes. Background indicates random block assignment. Original data contains many missing values. Missing values are imputed using the methods described above. }
   \label{gene_enrichment}
\end{figure}

Additionally, we note that genes assigned to multiple blocks may be involved in several important pathways in the sequenced cells. Since our dataset contains cells sequenced from a tumor of a patient with AML, we expect that genes involved in several pathways may be involved in pathways related to tumor progression and cancer. 35 of the top 1000 genes were assigned to 3 or more blocks. We performed gene set enrichment on this set of 35 genes and found that they were significantly enriched for genes related to Acute Myeloid Leukemia, Breast Cancer, Multiple Myeloma, and Myelogenous Leukemia (as shown in table \ref{gene_sets}). \\
\\
One gene, ARG1 was associated with 5 of the 10 blocks. A recent study published in 2016 showed that ARG1 was significantly overexpressed in blood-derived myeloid cells of breast cancer patient compared to healthy controls [7]. Thus suggests that ARG1 plays a role in cancer progression in blood-derived myeloid cells (the cells that were sequenced to obtain our data). However, ARG1 has never been studied in relationship to AML. Our analysis suggests that ARG1 may be an excellent candidate gene related to the development or progression of the AML phenotype. \\

\begin{figure}[h]
  \centering
    \includegraphics[width=.7\textwidth]{figures/gene_sets}
   \caption{Significantly enriched gene sets related to cancer and myeloid leukemia.}
   \label{gene_sets}
\end{figure}

\section{Breakdown of work}
Rebecca 
\begin{enumerate}
\item Obtained the data and performed initial data preparation, obtaining transcript counts and most variable genes from the raw sequencing files. 
\item Ran the GRAB model to obtain overlapping blocks
\item Analyzed results
\end{enumerate}
Sorin
\begin{enumerate}
\item Imputation of missing data
\item Selection of $K$ using elbow and silhouette methods (also attempted to implement Marlin and Murphy's Method)
\item Making scripts to run and analyze Schmidt code with different options and visualizing the learned graph structure
\end{enumerate}
\section{References}
\begin{enumerate}
\item Ding, Jiarui, et al. "Systematic analysis of somatic mutations impacting gene expression in 12 tumour types." Nature communications 6 (2015).
\item Patel, Anoop P., et al. "Single-cell RNA-seq highlights intratumoral heterogeneity in primary glioblastoma." Science 344.6190 (2014): 1396-1401.
\item Zheng, Grace XY, et al. "Massively parallel digital transcriptional profiling of single cells." bioRxiv (2016): 065912. [4]Zheng, Grace XY, et al. "Massively parallel digital transcriptional profiling of single cells." bioRxiv (2016): 065912. [5] Baladandayuthapani, Veerabhadran, et al. "Bayesian sparse graphical models for classification with application to protein expression data." The annals of applied statistics 8.3 (2014): 1443.
\item Hosseini, Seyed Mohammad Javad, and Su-In Lee. "Learning Sparse Gaussian Graphical Models with Overlapping Blocks." Advances In Neural Information Processing Systems. 2016.
\item Marlin, Benjamin M., and Kevin P. Murphy. "Sparse Gaussian graphical models with unknown block structure." Proceedings of the 26th Annual International Conference on Machine Learning. ACM, 2009.
\item Schmidt, Mark. Graphical model structure learning with l1-regularization. Diss. UNIVERSITY OF BRITISH COLUMBIA (Vancouver, 2010.
\item de Boniface, Jana, et al. "Expression patterns of the immunomodulatory enzyme arginase 1 in blood, lymph nodes and tumor tissue of early-stage breast cancer patients." Oncoimmunology 1.8 (2012): 1305-1312.
\item Peshkin, Leon, ``MATLAB - GraphViz interface.'' \\ 
\url{https://www.mathworks.com/matlabcentral/fileexchange/4518-matlab-graphviz-interface}
\end{enumerate}

\section{Appendix}
\subsection{Histograms of Data}
\begin{figure}[h]
  \centering
    \includegraphics[scale=.5]{figures/gene_sparsity_histogram}
   \caption{Histogram of sparsity (\% of values that are missing) of data.}
   \label{gene_sparsity_histogram}
\end{figure}
\begin{figure}[h]
  \centering
    \includegraphics[scale=.5]{figures/gene_count_histogram}
   \caption{Histogram of expression counts per gene.}
   \label{gene_count_histogram}
\end{figure}
\begin{figure}[h]
  \centering
    \includegraphics[scale=.5]{figures/cell_count_histogram}
   \caption{Histogram of the expression counts per cell.}
   \label{cell_count_histogram}
\end{figure}

\newpage
\subsection{Imputation Graphs}
\begin{figure}[h]
  \centering
    \includegraphics[scale=.5]{figures/gene10_mean_imputation_comparison}
   \caption{Histogram Before (top) And After (bottom) Mean Imputing missing values for Gene 10.}
   \label{mean_imputation_10}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[scale=.5]{figures/gene56_mean_imputation_comparison}
   \caption{Histogram Before (top) And After (bottom) Mean Imputing missing values for Gene 56.}
   \label{mean_imputation_56}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[scale=.5]{figures/gene10_imputation_comparison}
   \caption{Histogram Before (top) And After (bottom) Group Imputing missing values for Gene 10.}
   \label{group_imputation_10}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[scale=.5]{figures/gene330_imputation_comparison}
   \caption{Histogram Before (top) And After (bottom) Group Imputing missing values for Gene 330.}
   \label{group_imputation_330}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[scale=.5]{figures/cdf_gene10_mean_impute_plots}
   \caption{CDF Plots Before (top) And After (bottom) Mean Imputing missing values for Gene 10.}
   \label{mean_imputation_cdf_10}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[scale=.5]{figures/cdf_gene12_mean_impute_plots}
   \caption{CDF Plots Before (top) And After (bottom) Mean Imputing missing values for Gene 12.}
   \label{cdf_mean_imputation_12}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[scale=.5]{figures/cdf_gene10_imputed_plots}
   \caption{CDF Plots Before (top) And After (bottom) Group Imputing missing values for Gene 10.}
   \label{cdf_group_imputation_10}
\end{figure}

\begin{figure}[h]
  \centering
    \includegraphics[scale=.5]{figures/cdf_gene12_imputed_plots}
   \caption{CDF Plots Before (top) And After (bottom) Group Imputing missing values for Gene 12.}
   \label{cdf_group_imputation_12}
\end{figure}


\end{document}