function [adj] = plot_undirected_graph(edgeWeights, numNodes)
    addpath(genpath('graphviz'));
    [adj] = create_adjacency_matrix(edgeWeights, numNodes);
    graph_draw(adj);
end