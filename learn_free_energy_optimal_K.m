function [K, assignments] = learn_free_energy_optimal_K(data, maxIters)
    % data is NxD, N is # of samples, D is # of variables
    params = init_params(data);
    
    has_converged = 1;
    numIters = 0;
    while has_converged && numIters < maxIters
        has_converged = 0;
        params = update_variational_parameters(params);
        
        for k=1:params.K
            % try splitting by spectral clustering group k into two groups
            H = compute_similarity_matrix(params, k);
            cluster_split = SpectralClustering(H, 2); 
            new_params = split_cluster(params, cluster_split, k, params.K+1);
            [improved,new_params] = look_ahead_variational(new_params);
            
            if improved
                params = new_params;
                numIters = numIters + params.lookAhead;
                has_converged = 1;
                break;
            end
        end
        
        numIters = numIters + 1;
    end
    
    K = params.K;
    assignments = params.assignments;
end

%% free energy
function [free_energy] = compute_free_energy(params)
end

function [params] = update_variational_parameters(params)
    
    params.free_energy = compute_free_energy(params);
end

function [improved,new_params] = look_ahead_variational(params)
    new_params = params;
    lookAhead = new_params.lookAhead;
    
    for i=1:lookAhead
        new_params = update_variational_parameters(new_params);
    end
    
    if new_params.free_energy > params.free_energy
        improved = 1;
    else
        improved = 0;
    end
end

function [sim_matrix] = compute_similarity_matrix(params, k)
    in_cluster_k = (params.assignments == k);
    W = abs(params.W);
    sim_matrix = W(in_cluster_k,in_cluster_k) + 0.5 * W(in_cluster_k,~in_cluster_k) * W(in_cluster_k,~in_cluster_k)';
end

% split params.assignments == k into k,new_group based on assignments
function [params] = split_cluster(params, assignments, k, new_group)
    assert(sum(params.assignments == k) == len(assignments));
    
    new_assignments = assignments;
    new_assignments(new_assignments == 1) = k;
    new_assignments(new_assignments == 2) = new_group;
    params.assignments(params.assignments == k) = new_assignments;
    
    new_theta = zeros(params.K + 1, 1);
    new_pi = zeros(params.K + 1, params.K + 1);
    new_theta(1:params.K) = params.theta;
    new_pi(1:params.K,1:params.K) = params.pi;
    params.theta = new_theta;
    params.pi = new_pi;
    
    params.K = params.K + 1;
    params.a = 1 + (~eye(params.K));
    params.b = 1 + eye(params.K);
end

function [params] = init_params(data)
    params.lookAhead = 20;
    params.data = data;
    params.N = size(data,1);
    params.D = size(data,2); D = params.D;
    params.K = 1; 
    
    % hyperparameters
    params.alpha = 1;
    params.epsilon = 1;
    params.delta = 0.01;
    params.rho = 0.5;
    params.sigma = 0.5;
    params.a = 1 + (~eye(1));
    params.b = 1 + eye(1);
    
    % initial samples
    params.assignments = ones(D,1); % group assignments of each variable
    params.theta = zeros(params.K,1); % fraction of nodes in each group
    params.pi = ones(params.K,params.K); % probability of an edge between two groups
    params.G = ones(D,D); % edges between variables i,j
    params.sig0 = 1;
    params.sig1 = params.rho * params.sig0;
    params.W = zeros(D,D);
    params.X = zeros(D,N);
end